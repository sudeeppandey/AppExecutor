package pandey.sudeep.hamer;


import android.app.Activity;
import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

/**
 * A simple {@link Fragment} subclass.
 */
public class ExecutorFragment extends Fragment {

    interface ActivityCallbacks{
        void updateTextView();
        void signalEnd();
    }

    private ActivityCallbacks callbacks;
    private AppExecutors executors = new AppExecutors();
    private static final String TAG = ExecutorFragment.class.getSimpleName();

    public ExecutorFragment() {
        // Required empty public constructor
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        callbacks = (ActivityCallbacks) context;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        // Retain this fragment across configuration changes.
        setRetainInstance(true);

        // Create and execute the background task.

    }

    @Override
    public void onDetach() {
        super.onDetach();
        callbacks = null;
    }

    public void startExecutor(){
        executors.diskIO().execute(new Runnable() {
            @Override
            public void run() {

                int update = 0;
                for(int i=0;i<100;i++){
                    try {
                        Thread.sleep(300);
                        update+=1;
                        //final int finalUpdate = update;
                        executors.mainThread().execute(new Runnable() {
                            @Override
                            public void run() {
                                while(callbacks==null){
                                    continue;
                                }

                                callbacks.updateTextView();
                            }
                        });


                    } catch (InterruptedException e) {
                        //e.printStackTrace();
                    }

                    Log.d(TAG, "AppExecutors.execute() - executor in action...."+update+"....");
                }
                executors.mainThread().execute(new Runnable() {
                    @Override
                    public void run() {
                        while(callbacks==null){
                        continue;
                        }
                        callbacks.signalEnd();
                    }
                });
            }


        });
    }



}
