package pandey.sudeep.hamer;

import android.app.Activity;
import android.os.Handler;
import android.os.HandlerThread;
import android.os.Looper;
import android.os.Message;
import android.renderscript.RenderScript;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import static android.os.Process.THREAD_PRIORITY_DEFAULT;

public class MainActivity extends AppCompatActivity implements ExecutorFragment.ActivityCallbacks{

    private static final String TAG = MainActivity.class.getSimpleName();

    private static final String TAG_TASK_FRAGMENT = "task_fragment";

    private ExecutorFragment mTaskFragment;

    //private HandlerThread thread;
    //private MyHandler handler;
    //private Looper looper;
    private AppExecutors executors = new AppExecutors();

    private Button button;
    private TextView textView;
    private TextView textView2;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        button = findViewById(R.id.button);
        textView = findViewById(R.id.textView);
        textView2 = findViewById(R.id.textView2);

        android.support.v4.app.FragmentManager fm = getSupportFragmentManager();
        mTaskFragment = (ExecutorFragment) fm.findFragmentByTag(TAG_TASK_FRAGMENT);

        // If the Fragment is non-null, then it is currently being
        // retained across a configuration change.
        if (mTaskFragment == null) {
            mTaskFragment = new ExecutorFragment();
            fm.beginTransaction().add(mTaskFragment, TAG_TASK_FRAGMENT).commit();
        }
        if(savedInstanceState!=null){
            int num = (int)savedInstanceState.get("val");
            textView.setText(Integer.toString(num));
        }





        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
               // handler.handleMessage(Message.obtain());
                mTaskFragment.startExecutor();

            }
        });


    }
    @Override
    protected void onResume(){
        super.onResume();

    }

    @Override
    public void updateTextView(){
        int start = Integer.parseInt(textView.getText().toString());
        start+=1;
        textView.setText(Integer.toString(start));
        Log.d(TAG, "MainActivity.updateTextView() - TextView updated...."+start+"....");
    }
    @Override
    public void signalEnd(){
        textView2.setText("Complete");
        Log.d(TAG, "MainActivity.signalEnd() - TextView updated....COMPLETE....");
    }

    @Override
    public void onSaveInstanceState(Bundle outState){
        super.onSaveInstanceState(outState);
        outState.putInt("val",Integer.parseInt(textView.getText().toString()));

    }
}
